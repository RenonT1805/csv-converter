/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/RenonT1805/csv-converter/template"
)

type Options struct {
	CSV          string `validate:"required"`
	Template     string `validate:"required"`
	Out          string
	SeparateChar string `validate:"len=1"`
	Format       string
}

var (
	options = &Options{}
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "csv-converter",
	Short: "テンプレートファイルを元に、csvデータからテキストを生成するツール",
	Long:  ``,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
	PreRunE: func(cmd *cobra.Command, args []string) error {
		return validateParams(*options)
	},
	Run: run,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.Flags().StringVarP(&options.Out, "out", "o", "", "変換結果の出力ファイルのパスを指定します")
	rootCmd.Flags().StringVarP(&options.CSV, "csv", "c", "", "変換元のCSVファイルのパスを指定します(必須)")
	rootCmd.Flags().StringVarP(&options.Template, "template", "t", "", "変換元のテンプレートファイルを指定します(必須)")
	rootCmd.Flags().StringVarP(&options.SeparateChar, "separate", "s", ",", "CSVの分割文字を設定します")
	rootCmd.Flags().StringVarP(&options.Format, "format", "f", "", "テンプレートファイルのフォーマットを指定します")
}

func run(cmd *cobra.Command, args []string) {
	csvFile, err := os.Open(options.CSV)
	if err != nil {
		logrus.Errorln(err)
		os.Exit(1)
	}
	defer csvFile.Close()

	r := csv.NewReader(csvFile)
	r.Comma = rune(options.SeparateChar[0])

	csvMap := map[string][]string{}
	headers, err := r.Read()
	var dataCount int
	if err != nil {
		logrus.Infoln("CSVファイルの読み込みに失敗しました")
		os.Exit(1)
	}
	for _, header := range headers {
		csvMap[header] = []string{}
	}

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logrus.Infoln("CSVデータの読み込みに失敗しました")
			os.Exit(1)
		}

		for idx, item := range record {
			csvMap[headers[idx]] = append(csvMap[headers[idx]], item)
		}
		dataCount++
	}

	templateFile, err := os.Open(options.Template)
	if err != nil {
		logrus.Errorln(err)
		os.Exit(1)
	}
	defer templateFile.Close()

	templateSource, err := ioutil.ReadAll(templateFile)
	if err != nil {
		logrus.Errorln(err)
		os.Exit(1)
	}

	result, err := template.GenerateFromTemplate(csvMap, dataCount, string(templateSource), template.StringToTemplateFormat(options.Format))
	if err != nil {
		logrus.Errorln(err)
		os.Exit(1)
	}

	if options.Out == "" {
		_, err = fmt.Fprintf(logrus.StandardLogger().Out, "\n%s\n", result)
	} else {
		err = os.WriteFile(options.Out, []byte(result), 0666)
	}
	if err != nil {
		logrus.Errorln(err)
		os.Exit(1)
	}
}
