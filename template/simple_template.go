package template

import (
	"strings"
)

const (
	RepeatBeginSymbol string = "{{@@"
	RepeatEndSymbol   string = "@@}}"
)

func generateFromSimpleTemplate(csvData map[string][]string, dataCount int, templateSource string) string {
	var (
		start        int = strings.Index(templateSource, RepeatBeginSymbol)
		end          int = strings.Index(templateSource, RepeatEndSymbol)
		tmpStart     int
		tmpEnd       int
		replacable   string
		repeatResult string
		tmp          string
	)
	if start == -1 {
		start = 0
	} else {
		tmpStart = start + len(RepeatBeginSymbol)
	}
	if end == -1 {
		end = len(templateSource)
	} else {
		tmpEnd = end
		end += len(RepeatEndSymbol)
	}

	tmp = templateSource[tmpStart:tmpEnd]
	for i := 0; i < dataCount; i++ {
		buf := tmp
		for key := range csvData {
			buf = strings.ReplaceAll(buf, "{{"+key+"}}", csvData[key][i])
		}
		repeatResult += buf
	}

	replacable += templateSource[:start]
	replacable += repeatResult
	replacable += templateSource[end:]

	return replacable
}
