package template

import "strings"

type TemplateFormat int

const (
	InvalidFormat TemplateFormat = iota + 1
	SimpleTemplate
	GoTemplate
)

func StringToTemplateFormat(source string) TemplateFormat {
	switch strings.ToUpper(source) {
	case "", "SIMPLE", "SIMPLETEMPLATE":
		return SimpleTemplate
	case "GO", "GOTEMPLATE":
		return GoTemplate
	}
	return InvalidFormat
}
