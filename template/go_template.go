package template

import (
	"bytes"
	"text/template"
)

func generateFromGoTemplate(csvData map[string][]string, dataCount int, templateSource string) (string, error) {
	tmpl, err := template.New("tmpl").Funcs(template.FuncMap{
		"repeat": func(n int) []int {
			var res []int
			for i := 0; i < n; i++ {
				res = append(res, i)
			}
			return res
		},
		"get": func(name string, n int) string {
			if d, exist := csvData[name]; exist {
				return d[n]
			}
			return ""
		},
		"last": func(x int) bool {
			return x == dataCount-1
		},
	}).Parse(templateSource)
	if err != nil {
		return "", err
	}

	buf := &bytes.Buffer{}
	err = tmpl.Execute(buf, map[string]interface{}{
		"DataCount": dataCount,
		"CSVData":   csvData,
	})
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}
